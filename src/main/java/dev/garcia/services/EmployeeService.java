package dev.garcia.services;

import java.util.ArrayList;
import java.util.Scanner;

import dev.garcia.daos.BankAccountDao;
import dev.garcia.daos.BankAccountDaoImpl;
import dev.garcia.models.bankAccount;

public class EmployeeService {
	static Scanner scanner = new Scanner(System.in);

	public void employeeFlow(){
		
		String selection;
		do{
			System.out.println("Welcome back employee! Would you like to (1) Approve or reject some account, (2) View a customer's bank account, or (3) End the session?");
			selection = scanner.next();
			switch(selection) {
			case "1":
				accountChecker();
				break;
			case "2":
				accountView();
				break;
			case "3":
				System.out.println("Goodbye!");
				break;
			default:
				System.out.println("Invalid input. Please try again.");
				break;
			}
		}while(selection.equals("3") != true);
	}
	
	
	public void accountChecker() {
		BankAccountDao ba = new BankAccountDaoImpl();
		
		System.out.println("Currently listing all pending accounts.");
		ba.showAllPendingAccounts();
		System.out.println("Please enter the Username of the account you wish to update.");
		String username = scanner.next();
		System.out.println("Now enter the corresponding BankID.");
		int bankID = scanner.nextInt();
		System.out.println("Would you like to Accept or Reject the account? Please type 'A' or 'R'");
		String status = scanner.next();
		if(status.equals("A")) {
			ba.updatePendingAccounts(username, "ACTIVE", bankID);
			System.out.println("Account switched to ACTIVE successfully");
		}	
		else if(status.equals("R")) {
			ba.updatePendingAccounts(username, "CLOSED", bankID);
			System.out.println("Account switched to CLOSED successfully");
		}
		else 
			System.out.println("Invalid selection! Please try again with a correct selection");
	}
	
	public void accountView() {
		BankAccountDao ba = new BankAccountDaoImpl();
		System.out.println("Please type the username of the customer's accounts you wish to view.");
		String username = scanner.next();
		if(ba.accountSize(username) > 0) {
			System.out.println("Here are the current accounts under this user!");
			ArrayList<bankAccount> accounts = ba.getAccounts(username);
			for (int i = 0; i < accounts.size(); i++)  
				System.out.print("["+(i+1)+"] " + accounts.get(i) + "\n");  
		}
		else
			System.out.println("That user currently doesn't exist. Please try again with a valid user.");
	}
	
}
