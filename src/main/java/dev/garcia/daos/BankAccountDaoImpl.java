package dev.garcia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dev.garcia.models.Customer;
import dev.garcia.models.bankAccount;
import dev.garcia.models.bankStatus;
import dev.garcia.util.ConnectionUtil;

public class BankAccountDaoImpl implements BankAccountDao{
	
	@Override
	public void deposit(String username, int accountSelection, double depositAmount){
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String getBankID = "SELECT BANK_ID FROM BANK_ACCOUNTS ba WHERE ba.USER_ACCOUNT = ?";
		String deposit = "UPDATE BANK_ACCOUNTS SET BALANCE = ? WHERE BANK_ID = ?";
				
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement bankIDStatement = connection.prepareStatement(getBankID);
				PreparedStatement depositStatement = connection.prepareStatement(deposit)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			bankIDStatement.setInt(1,accountID);
			ResultSet rs2 = bankIDStatement.executeQuery();
			int bankID = 0;
			int i = 1;
			while(rs2.next()) {
				if(i == accountSelection) 
					bankID = rs2.getInt("BANK_ID");
				i++;
			}
	
			depositStatement.setDouble(1, depositAmount);
			depositStatement.setInt(2, bankID);
			depositStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void withdrawal(String username, int accountSelection, double withdrawalAmount) {
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String getBankID = "SELECT BANK_ID FROM BANK_ACCOUNTS ba WHERE ba.USER_ACCOUNT = ?";
		String withdrawal = "UPDATE BANK_ACCOUNTS SET BALANCE = ? WHERE BANK_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement bankIDStatement = connection.prepareStatement(getBankID);
				PreparedStatement withdrawalStatement = connection.prepareStatement(withdrawal)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			bankIDStatement.setInt(1,accountID);
			ResultSet rs2 = bankIDStatement.executeQuery();
			int bankID = 0;
			int i = 1;
			while(rs2.next()) {
				if(i == accountSelection) 
					bankID = rs2.getInt("BANK_ID");
				i++;
			}
		
			withdrawalStatement.setDouble(1, withdrawalAmount);
			withdrawalStatement.setInt(2, bankID);
			withdrawalStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public double balanceView(String username, int accountSelection) {
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String getBankID = "SELECT BANK_ID FROM BANK_ACCOUNTS ba WHERE ba.USER_ACCOUNT = ?";
		String getBalance = "SELECT BALANCE FROM BANK_ACCOUNTS ba WHERE ba.BANK_ID = ?";
		double balanceAmount = 0;	
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement bankIDStatement = connection.prepareStatement(getBankID);
				PreparedStatement balanceStatement = connection.prepareStatement(getBalance)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			bankIDStatement.setInt(1,accountID);
			ResultSet rs2 = bankIDStatement.executeQuery();
			int bankID = 0;
			int i = 1;
			while(rs2.next()) {
				if(i == accountSelection) 
					bankID = rs2.getInt("BANK_ID");
				i++;
			}
			
			balanceStatement.setDouble(1, bankID);
			ResultSet rs3 = balanceStatement.executeQuery();
			rs3.next();
			balanceAmount = rs3.getDouble("BALANCE");
			rs3.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return balanceAmount;
	}
	
	@Override
	public void newUser(String username, String password, double balanceAmount) {
		String customerInsert = "INSERT INTO CUSTOMER VALUES (?,?)";
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String accountInsert = "INSERT INTO BANK_ACCOUNTS (USER_ACCOUNT, BANK_STATUS, BALANCE) VALUES (?,'PENDING',?)";
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement customerStatement = connection.prepareStatement(customerInsert);
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement accountStatement = connection.prepareStatement(accountInsert)){
			
			customerStatement.setString(1, username);
			customerStatement.setString(2, password);
			customerStatement.execute();
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			accountStatement.setInt(1, accountID);
			accountStatement.setDouble(2, balanceAmount);
			accountStatement.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void newAccount(String username, double balanceAmount) {
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String accountInsert = "INSERT INTO BANK_ACCOUNTS (USER_ACCOUNT, BANK_STATUS, BALANCE) VALUES (?,'PENDING',?)";
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement accountStatement = connection.prepareStatement(accountInsert)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			accountStatement.setInt(1, accountID);
			accountStatement.setDouble(2, balanceAmount);
			accountStatement.execute();
		
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getStatus(String username, int accountSelection) {
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String getBankID = "SELECT BANK_ID FROM BANK_ACCOUNTS ba WHERE ba.USER_ACCOUNT = ?";
		String getPending = "SELECT BANK_STATUS FROM BANK_ACCOUNTS ba WHERE ba.BANK_ID = ?";
		String status = null;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement bankIDStatement = connection.prepareStatement(getBankID);
				PreparedStatement pendingStatement = connection.prepareStatement(getPending)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			bankIDStatement.setInt(1,accountID);
			ResultSet rs2 = bankIDStatement.executeQuery();
			int bankID = 0;
			int i = 1;
			while(rs2.next()) {
				if(i == accountSelection) 
					bankID = rs2.getInt("BANK_ID");
				i++;
			}
			
			pendingStatement.setDouble(1, bankID);
			ResultSet rs3 = pendingStatement.executeQuery();
			rs3.next();
			status = rs3.getString("BANK_STATUS");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			return status;
	}
	
	@Override
	public void showAllPendingAccounts(){
		String getPendingAccounts = "SELECT c.USERNAME , ba.BANK_ID , ba.BALANCE , ba.BANK_STATUS FROM CUSTOMER c FULL JOIN BANK_ACCOUNTS ba ON c.ACCOUNT_NUMBER = ba.USER_ACCOUNT WHERE ba.BANK_STATUS = 'PENDING'";
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement getPendingAccountsStatement = connection.prepareStatement(getPendingAccounts);){
			String username = null;
			int bankID;
			double balanceAmount;
			String bankStatus = null;
			int counter = 1;
			ResultSet rs = getPendingAccountsStatement.executeQuery();
			while(rs.next()) {
			if(rs.getString("USERNAME") != username)
				username = rs.getString("USERNAME");
			bankID = rs.getInt("BANK_ID");
			balanceAmount = rs.getDouble("BALANCE");
			bankStatus = rs.getString("BANK_STATUS");
			System.out.println(counter + " Username: " + username + " BankID: " + bankID + " Balance Amount: " + balanceAmount + " Bank Status: " + bankStatus + "\n");
			counter++;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void updatePendingAccounts(String username, String status, int bankID){
		String getPendingAccounts = "UPDATE MY_VIEW SET my_view.BANK_STATUS = ? WHERE my_view.BANK_ID = ? AND my_view.USERNAME = ?";
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pendingAccountsStatement = connection.prepareStatement(getPendingAccounts)){
			
			pendingAccountsStatement.setString(1, status);
			pendingAccountsStatement.setInt(2, bankID);
			pendingAccountsStatement.setString(3, username);
			pendingAccountsStatement.execute();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public int accountSize(String username) {
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String getAccountSize = "SELECT COUNT (*) FROM BANK_ACCOUNTS ba WHERE ba.USER_ACCOUNT = ?";
		int accountSize = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement accountSizeStatement = connection.prepareStatement(getAccountSize)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			accountSizeStatement.setInt(1,accountID);
			ResultSet rs2 = accountSizeStatement.executeQuery();
			rs2.next();
			accountSize = rs2.getInt("COUNT(*)");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return accountSize;
	}
	
	@Override
	public ArrayList<bankAccount> getAccounts(String username){
		String getAccountID = "SELECT ACCOUNT_NUMBER FROM CUSTOMER WHERE CUSTOMER.USERNAME = ?";
		String getAccounts = "SELECT BANK_STATUS, BALANCE FROM BANK_ACCOUNTS ba WHERE ba.USER_ACCOUNT = ?";
		ArrayList<bankAccount> accounts = new ArrayList<bankAccount>();

		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement accountIDStatement = connection.prepareStatement(getAccountID);
				PreparedStatement accountsStatement = connection.prepareStatement(getAccounts)){
			
			accountIDStatement.setString(1, username);
			ResultSet rs = accountIDStatement.executeQuery();
			rs.next();
			int accountID = rs.getInt("ACCOUNT_NUMBER");
			
			accountsStatement.setInt(1,accountID);
			ResultSet rs2 = accountsStatement.executeQuery();
			while(rs2.next()) {
				bankAccount ba = new bankAccount();
				ba.setBalance(rs2.getDouble("BALANCE"));
				ba.setStatus(bankStatus.valueOf(rs2.getString("BANK_STATUS")));
				accounts.add(ba);
			}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return accounts;
}
	
}
