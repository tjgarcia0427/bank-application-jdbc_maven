package dev.garcia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dev.garcia.util.ConnectionUtil;

public class UserDaoImpl implements UserDao {
	public boolean isExistingCustomer(String username, String password) {
		String getCustomerNum = "SELECT COUNT (*) FROM CUSTOMER c WHERE c.USERNAME = ? AND c.PASSWORD = ?";
		int customerNum = 0;
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement customerNumStatement = connection.prepareStatement(getCustomerNum)){
			customerNumStatement.setString(1, username);
			customerNumStatement.setString(2, password);
			ResultSet rs = customerNumStatement.executeQuery();
			rs.next();
			customerNum = rs.getInt("COUNT(*)");
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(customerNum == 1)
			return true;
		else
			return false;
	}
	public boolean isNewCustomer(String username, String password) {
		String getCustomerNum = "SELECT COUNT (*) FROM CUSTOMER c WHERE c.USERNAME = ? AND c.PASSWORD = ?";
		int customerNum = 0;
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement customerNumStatement = connection.prepareStatement(getCustomerNum)){
			customerNumStatement.setString(1, username);
			customerNumStatement.setString(2, password);
			ResultSet rs = customerNumStatement.executeQuery();
			rs.next();
			customerNum = rs.getInt("COUNT(*)");
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(customerNum == 0 && isEmployee(username, password) == false)
			return true;
		else
			return false;
	}
	public boolean isEmployee(String username, String password) {
		String getEmployeeNum = "SELECT COUNT (*) FROM EMPLOYEE e WHERE e.USERNAME = ? AND e.PASSWORD = ?";
		int employeeNum = 0;
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement employeeNumStatement = connection.prepareStatement(getEmployeeNum)){
			employeeNumStatement.setString(1, username);
			employeeNumStatement.setString(2, password);
			ResultSet rs = employeeNumStatement.executeQuery();
			rs.next();
			employeeNum = rs.getInt("COUNT(*)");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(employeeNum == 1)
			return true;
		else
			return false;
		
	}
}
