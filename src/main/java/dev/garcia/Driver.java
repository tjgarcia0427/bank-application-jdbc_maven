package dev.garcia;

import java.util.Scanner;

import dev.garcia.daos.UserDao;
import dev.garcia.daos.UserDaoImpl;
import dev.garcia.services.CustomerService;
import dev.garcia.services.EmployeeService;

public class Driver {
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		UserDao userDao = new UserDaoImpl();
		CustomerService cs = new CustomerService();
		EmployeeService es = new EmployeeService();
		String selection;
		do {
		System.out.println("Welcome to our Banking App!");
		System.out.println("Please log in with your username and password. You will be directed to your corresponding portal based on your credentials.");
		System.out.println("Username: ");
		String username = scanner.next();
		System.out.println("Password: ");
		String password = scanner.next();
		
			if(userDao.isNewCustomer(username, password) == true) {
				cs.newCustomerFlow(username,password);
			}
			else if(userDao.isExistingCustomer(username, password) == true) {
				cs.existingCustomerFlow(username);
			}
			else if(userDao.isEmployee(username, password) == true) {
				es.employeeFlow();
			}
		
		System.out.println("Would you like to close your session? (Y/N)");
		selection = scanner.next();
		}while(selection.equals("N"));
		System.out.println("Logging out...");
	}
	
	
}
