package dev.garcia.models;

import java.util.ArrayList;

public class Customer extends User {
	
	public ArrayList<bankAccount> currAccounts = new ArrayList<bankAccount>();
	
	public Customer(String username, String password) {
		super(username, password);
	}
	
	public Customer(String username, String password, ArrayList<bankAccount> currAccounts ) {
		super(username, password);
		this.currAccounts = currAccounts;
	}

	@Override
	public String toString() {
		return "Customer [username=" + username + ", currAccounts=" + currAccounts + "]";
	}
	
	
}
