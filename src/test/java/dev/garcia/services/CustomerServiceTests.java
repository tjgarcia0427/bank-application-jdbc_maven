package dev.garcia.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CustomerServiceTests {
	
	private CustomerService customerService = new CustomerService();
	
	@Test
	public void testWithdrawalWithNegative(){
		boolean expected = false;
		boolean actual = customerService.validWithdrawal(100.00, -5);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithdrawalWithZero(){
		boolean expected = false;
		boolean actual = customerService.validWithdrawal(100.00,0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithdrawalWithInsufficent(){
		boolean expected = false;
		boolean actual = customerService.validWithdrawal(100.00, 200.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithdrawalWithValid(){
		boolean expected = true;
		boolean actual = customerService.validWithdrawal(100.00, 50.00);
		assertEquals(expected, actual);
	}

	@Test
	public void testDepositWithZero(){
		boolean expected = false;
		boolean actual = customerService.validDeposit(0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testDepositWithNegative(){
		boolean expected = false;
		boolean actual = customerService.validDeposit(-100.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testDepositWithValid(){
		boolean expected = true;
		boolean actual = customerService.validDeposit(100.00);
		assertEquals(expected, actual);
	}
}
